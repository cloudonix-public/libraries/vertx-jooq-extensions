package io.cloudonix.lib.model.errors;

public class NoSuchRecord extends Exception {

	private static final long serialVersionUID = 1315849923704443174L;
	private String missingType = "";

	public NoSuchRecord() {
		super("Requested record was not found");
	}

	public NoSuchRecord(String missingType) {
		super("Requested record of type " + missingType + " was not found");
		this.missingType = missingType;
	}
	
	public String getMissingRecordType() {
		return missingType;
	}
}
