package io.cloudonix.lib.model.support;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

import org.jooq.Binding;
import org.jooq.BindingGetResultSetContext;
import org.jooq.BindingGetSQLInputContext;
import org.jooq.BindingGetStatementContext;
import org.jooq.BindingRegisterContext;
import org.jooq.BindingSQLContext;
import org.jooq.BindingSetSQLOutputContext;
import org.jooq.BindingSetStatementContext;
import org.jooq.Converter;
import org.jooq.impl.DefaultBinding;

public class TimestampBinding implements Binding<Timestamp, Instant> {
	private static final long serialVersionUID = 5919888480637651839L;

	static public class TimestampConverter implements Converter<Timestamp, Instant> {
		private static final long serialVersionUID = 7558984542754629742L;

		@Override
		public Instant from(Timestamp ts) {
			return ts == null ? null : ts.toInstant();
		}

		@Override
		public Timestamp to(Instant instant) {
			return instant == null ? null : Timestamp.from(instant);
		}

		@Override
		public Class<Timestamp> fromType() {
			return Timestamp.class;
		}

		@Override
		public Class<Instant> toType() {
			return Instant.class;
		}
	}

	private static final Converter<Timestamp, Instant> converter = new TimestampConverter();

	private final Binding<Timestamp, Instant> delegate = DefaultBinding.binding(converter);

	/**
	 * @return
	 * @see org.jooq.Binding#converter()
	 */
	public Converter<Timestamp, Instant> converter() {
		return delegate.converter();
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#sql(org.jooq.BindingSQLContext)
	 */
	public void sql(BindingSQLContext<Instant> ctx) throws SQLException {
		delegate.sql(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#register(org.jooq.BindingRegisterContext)
	 */
	public void register(BindingRegisterContext<Instant> ctx) throws SQLException {
		delegate.register(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#set(org.jooq.BindingSetStatementContext)
	 */
	public void set(BindingSetStatementContext<Instant> ctx) throws SQLException {
		delegate.set(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#set(org.jooq.BindingSetSQLOutputContext)
	 */
	public void set(BindingSetSQLOutputContext<Instant> ctx) throws SQLException {
		delegate.set(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#get(org.jooq.BindingGetResultSetContext)
	 */
	public void get(BindingGetResultSetContext<Instant> ctx) throws SQLException {
		delegate.get(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#get(org.jooq.BindingGetStatementContext)
	 */
	public void get(BindingGetStatementContext<Instant> ctx) throws SQLException {
		delegate.get(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#get(org.jooq.BindingGetSQLInputContext)
	 */
	public void get(BindingGetSQLInputContext<Instant> ctx) throws SQLException {
		delegate.get(ctx);
	}

}
