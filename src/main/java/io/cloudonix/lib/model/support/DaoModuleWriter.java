package io.cloudonix.lib.model.support;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.jooq.codegen.GeneratorStrategy;
import org.jooq.codegen.GeneratorStrategy.Mode;
import org.jooq.codegen.JavaWriter;
import org.jooq.meta.SchemaDefinition;
import org.jooq.meta.TableDefinition;
import org.jooq.tools.JooqLogger;

import io.github.jklingsporn.vertx.jooq.generate.VertxJavaWriter;

public class DaoModuleWriter extends VertxJavaWriter {

	private Map<String, String> m_concretePojoMapping = new HashMap<>();
	private boolean withJakartaEE = ExtendedVertxGeneratorStrategy.withJakartaEE();
	
	private static final JooqLogger log = JooqLogger.getLogger(DaoModuleWriter.class);

	public DaoModuleWriter(File file, String generateFullyQualifiedTypes, String targetEncoding, SchemaDefinition schema, GeneratorStrategy strategy) {
		super(file, generateFullyQualifiedTypes, targetEncoding);
		ExtendedVertxGeneratorStrategy myCustomStrategy = new ExtendedVertxGeneratorStrategy(strategy);
		for (TableDefinition table : schema.getTables()) {
			String source = strategy.getFullJavaClassName(table, Mode.POJO);
			if (!myCustomStrategy.hasConcreteClass(table, Mode.POJO))
				continue;
			String target = myCustomStrategy.getConcreteFullJavaClassName(table, Mode.POJO);
			log.info("Overriding output type for " + table + " to concrete implementation " + target);
			m_concretePojoMapping.put(source, target);
		}
	}

	@Override
	public String ref(String clazzOrId) {
		return super.ref(m_concretePojoMapping.getOrDefault(clazzOrId, clazzOrId));
	}

	@Override
	public List<String> ref(List<String> clazzOrIds) {
		return super.ref(Objects.isNull(clazzOrIds) ? null
				: clazzOrIds.stream()
						.map(s -> m_concretePojoMapping.getOrDefault(s,s))
						.collect(Collectors.toList()));
	}

	@Override
	public JavaWriter print(String string, Object... args) {
		return super.print(replaceInText(string), replaceInArray(args));
	}

	private Object[] replaceInArray(Object[] args) {
		for (int i = 0; i < args.length; i++)
			if (args[i] instanceof String)
				args[i] = replaceInText((String) args[i]);
			else if (args[i] instanceof Collection) {
				args[i] = replaceInArray(((Collection<?>) args[i]).toArray());
			}
		return args;
	}

	private String replaceInText(String string) {
		if (withJakartaEE && string.contains("javax"))
			string = string.replaceAll("javax\\.inject\\.", "jakarta.inject.");
		return m_concretePojoMapping.getOrDefault(string, string);
	}
}
