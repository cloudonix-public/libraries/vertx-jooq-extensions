package io.cloudonix.lib.model.support;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.jooq.codegen.GeneratorStrategy;
import org.jooq.codegen.GeneratorStrategy.Mode;
import org.jooq.codegen.JavaWriter;
import org.jooq.meta.TableDefinition;
import org.jooq.tools.JooqLogger;

import io.github.jklingsporn.vertx.jooq.generate.VertxJavaWriter;

public class OverridableJavaWriter extends VertxJavaWriter {

	private String m_sourceType;
	private String m_targetType;
	private boolean withJakartaEE = ExtendedVertxGeneratorStrategy.withJakartaEE();
	
	private static final JooqLogger log = JooqLogger.getLogger(OverridableJavaWriter.class);

	public OverridableJavaWriter(File file, String generateFullyQualifiedTypes, String targetEncoding) {
		super(file, generateFullyQualifiedTypes, targetEncoding);
	}

	public void setTable(TableDefinition table, GeneratorStrategy st) {
		m_sourceType = st.getFullJavaClassName(table, Mode.POJO);
		String origPackage = st.getJavaPackageName(table, Mode.POJO); 
		if (!ExtendedVertxGeneratorStrategy.hasConcreteClass(st, origPackage, table, Mode.POJO))
			return;
		m_targetType = new ExtendedVertxGeneratorStrategy(st).getConcreteFullJavaClassName(table, Mode.POJO);
		log.info("Writer overrided output type for " + table + " to concrete implementation " + m_targetType);
	}
	
	@Override
	public String ref(String clazzOrId) {
		if (Objects.equals(m_sourceType, clazzOrId) && Objects.nonNull(m_targetType))
			return super.ref(m_targetType);
		return super.ref(clazzOrId);
	}

	@Override
	//TODO: I changed this from protected to public
	public List<String> ref(List<String> clazzOrIds) {
		return super.ref(Objects.isNull(clazzOrIds) ? null
				: clazzOrIds.stream()
						.map(s -> Objects.equals(m_sourceType, s) && Objects.nonNull(m_targetType) ? m_targetType : s)
						.collect(Collectors.toList()));
	}

	@Override
	public JavaWriter print(String string, Object... args) {
		return super.print(replaceInText(string), replaceInArray(args));
	}

	private Object[] replaceInArray(Object[] args) {
		for (int i = 0; i < args.length; i++)
			if (args[i] instanceof String)
				args[i] = replaceInText((String) args[i]);
			else if (args[i] instanceof Collection) {
				args[i] = replaceInArray(((Collection<?>) args[i]).toArray());
			}
		return args;
	}

	private String replaceInText(String string) {
		if (withJakartaEE && string.contains("javax"))
			string = string.replaceAll("javax\\.inject\\.", "jakarta.inject.");
		if (Objects.isNull(m_targetType))
			return string;
		if (!string.contains(m_sourceType))
			return string;
		System.err.println("Replacing " + m_sourceType + " with " + m_targetType + " in '" + string + "'");
		return string.replaceAll(m_sourceType, m_targetType);
	}
}
