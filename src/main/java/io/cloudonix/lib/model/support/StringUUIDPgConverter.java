package io.cloudonix.lib.model.support;

import java.util.UUID;

import org.jooq.Converter;

import io.github.jklingsporn.vertx.jooq.shared.postgres.PgConverter;
import io.github.jklingsporn.vertx.jooq.shared.postgres.RowConverter;

@SuppressWarnings("serial")
public class StringUUIDPgConverter implements PgConverter<String, String, UUID>{
	
	@Override
	public UUID from(String databaseObject) {
		return databaseObject == null ? null : UUID.fromString(databaseObject);
	}

	@Override
	public String to(UUID userObject) {
		return userObject == null ? null : userObject.toString();
	}

	@Override
	public Class<String> fromType() {
		return String.class;
	}

	@Override
	public Class<UUID> toType() {
		return UUID.class;
	}

	@Override
	public Converter<String, UUID> pgConverter() {
		return this;
	}

	@Override
	public RowConverter<String, UUID> rowConverter() {
		return new RowConverter<String, UUID>() {
			
			@Override
			public Class<UUID> toType() {
				return StringUUIDPgConverter.this.toType();
			}
			
			@Override
			public String to(UUID userObject) {
				return StringUUIDPgConverter.this.to(userObject);
			}
			
			@Override
			public Class<String> fromType() {
				return StringUUIDPgConverter.this.fromType();
			}
			
			@Override
			public UUID from(String databaseObject) {
				return StringUUIDPgConverter.this.from(databaseObject);
			}
		};
	}
}
