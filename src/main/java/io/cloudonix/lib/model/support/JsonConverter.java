package io.cloudonix.lib.model.support;

import io.github.jklingsporn.vertx.jooq.shared.postgres.PgConverter;

public interface JsonConverter<JSONTYPE, JAVATYPE> {
	PgConverter<JSONTYPE, JSONTYPE, JAVATYPE> jsonConverter();
}
