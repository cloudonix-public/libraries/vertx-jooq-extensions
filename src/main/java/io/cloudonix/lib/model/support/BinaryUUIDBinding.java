package io.cloudonix.lib.model.support;

import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.UUID;

import org.jooq.Binding;
import org.jooq.BindingGetResultSetContext;
import org.jooq.BindingGetSQLInputContext;
import org.jooq.BindingGetStatementContext;
import org.jooq.BindingRegisterContext;
import org.jooq.BindingSQLContext;
import org.jooq.BindingSetSQLOutputContext;
import org.jooq.BindingSetStatementContext;
import org.jooq.Converter;
import org.jooq.impl.DefaultBinding;

import io.github.jklingsporn.vertx.jooq.shared.postgres.PgConverter;

/**
 * Binary UUID binding from
 * https://www.internetstaff.com/mapping-mysql-binary-uuids-jooq/
 */
public class BinaryUUIDBinding implements Binding<byte[], UUID>, JsonConverter<String,UUID> {
	private static final long serialVersionUID = -5871748729008334129L;

	static public class ByteArrayToUUIDConverter implements Converter<byte[], UUID> {
		private static final long serialVersionUID = -236917451849972004L;

		@Override
		public final UUID from(byte[] t) {
			if (t == null) {
				return null;
			}

			ByteBuffer bb = ByteBuffer.wrap(t);
			return new UUID(fromMonotonic(bb.getLong()), bb.getLong());
		}

		@Override
		public final byte[] to(UUID u) {
			if (u == null) {
				return null;
			}

			return ByteBuffer.wrap(new byte[16]).putLong(toMonotonic(u.getMostSignificantBits()))
					.putLong(u.getLeastSignificantBits()).array();
		}
		
		/**
		 * Reorder the 64 MSB so version 1 UUIDs look to be monotonic 
		 * @param u UUID top half
		 * @return UUID top half reordered
		 */
		private long toMonotonic(long u) {
			return ByteBuffer.allocateDirect(8)
					.putShort((short) (u & 0xFFFF)) // 3rd group
					.putShort((short) ((u >> 16) & 0xFFFF)) // 2nd group
					.putInt((int)((u >> 32) & 0xFFFFFFFFL)) // 1st group
					.flip()
					.getLong();
		}
		
		/**
		 * Reorder the "monotonic"-styled UUID top half so it conforms to standard UUID
		 * @param u UUID top half
		 * @return UUID top half de-reordered
		 */
		private long fromMonotonic(long u) {
			return ByteBuffer.allocateDirect(8)
					.putInt((int)(u & 0xFFFFFFFFL))
					.putShort((short)((u >> 32) & 0xFFFF))
					.putShort((short)((u >> 48) & 0xFFFF))
					.flip()
					.getLong();
		}

		@Override
		public Class<byte[]> fromType() {
			return byte[].class;
		}

		@Override
		public Class<UUID> toType() {
			return UUID.class;
		}
	}
	
	private static final Converter<byte[], UUID> converter = new ByteArrayToUUIDConverter();

	private final Binding<byte[], UUID> delegate = DefaultBinding.binding(converter);

	/**
	 * @return
	 * @see org.jooq.Binding#converter()
	 */
	public Converter<byte[], UUID> converter() {
		return delegate.converter();
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#sql(org.jooq.BindingSQLContext)
	 */
	public void sql(BindingSQLContext<UUID> ctx) throws SQLException {
		delegate.sql(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#register(org.jooq.BindingRegisterContext)
	 */
	public void register(BindingRegisterContext<UUID> ctx) throws SQLException {
		delegate.register(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#set(org.jooq.BindingSetStatementContext)
	 */
	public void set(BindingSetStatementContext<UUID> ctx) throws SQLException {
		delegate.set(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#set(org.jooq.BindingSetSQLOutputContext)
	 */
	public void set(BindingSetSQLOutputContext<UUID> ctx) throws SQLException {
		delegate.set(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#get(org.jooq.BindingGetResultSetContext)
	 */
	public void get(BindingGetResultSetContext<UUID> ctx) throws SQLException {
		delegate.get(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#get(org.jooq.BindingGetStatementContext)
	 */
	public void get(BindingGetStatementContext<UUID> ctx) throws SQLException {
		delegate.get(ctx);
	}

	/**
	 * @param ctx
	 * @throws SQLException
	 * @see org.jooq.Binding#get(org.jooq.BindingGetSQLInputContext)
	 */
	public void get(BindingGetSQLInputContext<UUID> ctx) throws SQLException {
		delegate.get(ctx);
	}

	@Override
	public PgConverter<String, String, UUID> jsonConverter() {
		return new StringUUIDPgConverter();
	}

}
