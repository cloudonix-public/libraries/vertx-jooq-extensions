package io.cloudonix.lib.model.support;

import io.vertx.core.json.JsonObject;

import java.sql.Types;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;

import org.jooq.Binding;
import org.jooq.BindingGetResultSetContext;
import org.jooq.BindingGetSQLInputContext;
import org.jooq.BindingGetStatementContext;
import org.jooq.BindingRegisterContext;
import org.jooq.BindingSQLContext;
import org.jooq.BindingSetSQLOutputContext;
import org.jooq.BindingSetStatementContext;
import org.jooq.Converter;
import org.jooq.conf.ParamType;
import org.jooq.impl.DSL;

@SuppressWarnings("serial")
public class MySQLJsonVertxBinding implements Binding<Object, JsonObject> {

	@Override
	public Converter<Object, JsonObject> converter() {
		return new Converter<Object, JsonObject>() {
			@Override
			public JsonObject from(Object databaseObject) {
				return new JsonObject(String.valueOf(databaseObject));
			}

			@Override
			public Object to(JsonObject userObject) {
				return userObject.encode();
			}

			@Override
			public Class<Object> fromType() {
				return Object.class;
			}

			@Override
			public Class<JsonObject> toType() {
				return JsonObject.class;
			}
		};
	}

	@Override
	public void sql(BindingSQLContext<JsonObject> ctx) throws SQLException {
		if (ctx.render().paramType() == ParamType.INLINED)
			ctx.render().visit(DSL.inline(ctx.convert(converter()).value())).sql("::json");
		else
			ctx.render().sql("?::json");
	}

	@Override
	public void register(BindingRegisterContext<JsonObject> ctx) throws SQLException {
		ctx.statement().registerOutParameter(ctx.index(), Types.VARCHAR);
	}

	@Override
	public void set(BindingSetStatementContext<JsonObject> ctx) throws SQLException {
		ctx.statement().setObject(ctx.index(), ctx.convert(converter()).value());
	}

	@Override
	public void set(BindingSetSQLOutputContext<JsonObject> ctx) throws SQLException {
		throw new SQLFeatureNotSupportedException();
	}

	@Override
	public void get(BindingGetResultSetContext<JsonObject> ctx) throws SQLException {
		ctx.convert(converter()).value(ctx.resultSet().getString(ctx.index()));
	}

	@Override
	public void get(BindingGetStatementContext<JsonObject> ctx) throws SQLException {
		 ctx.convert(converter()).value(ctx.statement().getString(ctx.index()));
	}

	@Override
	public void get(BindingGetSQLInputContext<JsonObject> ctx) throws SQLException {
		throw new SQLFeatureNotSupportedException();
	}

}
