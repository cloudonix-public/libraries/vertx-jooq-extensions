package io.cloudonix.lib.model.support;

import org.jooq.Converter;
import org.jooq.impl.IdentityConverter;

import io.github.jklingsporn.vertx.jooq.shared.postgres.IdentityRowConverter;
import io.github.jklingsporn.vertx.jooq.shared.postgres.PgConverter;
import io.github.jklingsporn.vertx.jooq.shared.postgres.RowConverter;
import io.vertx.core.json.JsonObject;

public class JsonObjectLaxConverter extends io.github.jklingsporn.vertx.jooq.shared.JsonObjectConverter 
		implements PgConverter<JsonObject, String, JsonObject> {
	private static final long serialVersionUID = -6087953115481828402L;

	@Override
	public JsonObject from(String databaseObject) {
		return databaseObject == null ? null : databaseObject.isBlank() ? new JsonObject() : super.from(databaseObject);
	}

	@Override
	public Converter<JsonObject, JsonObject> pgConverter() {
		return new IdentityConverter<>(JsonObject.class);
	}

	@Override
	public RowConverter<JsonObject, JsonObject> rowConverter() {
		return new IdentityRowConverter<>(JsonObject.class);
	}
}
