package io.cloudonix.lib.model.support;

import java.io.File;

import org.jooq.codegen.GeneratorStrategy.Mode;
import org.jooq.codegen.JavaWriter;
import org.jooq.meta.DataTypeDefinition;
import org.jooq.meta.SchemaDefinition;
import org.jooq.meta.TableDefinition;
import org.jooq.meta.TypedElementDefinition;
import org.jooq.tools.JooqLogger;

import io.github.jklingsporn.vertx.jooq.generate.classic.ClassicJDBCGuiceVertxGenerator;

public class ExtendedClassicJDBCGuiceVertxGenerator extends ClassicJDBCGuiceVertxGenerator {
	private static final JooqLogger log = JooqLogger.getLogger(ExtendedClassicJDBCGuiceVertxGenerator.class);
	private SchemaDefinition m_currentSchema;

	@Override
	protected void generateDao(TableDefinition table, JavaWriter out) {
		if (out instanceof OverridableJavaWriter oout)
			oout.setTable(table, getStrategy());
		super.generateDao(table, out);
	}

	@Override
	protected JavaWriter newJavaWriter(File file, Mode mode) {
		log.info("Creating JavaWriter for  " + file.getName() + " in mode " + mode);
		switch (mode) {
		case DAO:
			return new DaoModuleWriter(file, generateFullyQualifiedTypes(), targetEncoding, m_currentSchema, getStrategy());
		default:
			return new OverridableJavaWriter(file, generateFullyQualifiedTypes(), targetEncoding);
		}
	}

	@Override
	protected JavaWriter newJavaWriter(File file) {
		log.info("Creating JavaWriter for  " + file.getName());
		if (file.getName().equals("DaoModule.java"))
			return new DaoModuleWriter(file, generateFullyQualifiedTypes(), targetEncoding, m_currentSchema, getStrategy());
		return new OverridableJavaWriter(file, generateFullyQualifiedTypes(), targetEncoding);
	}

	@Override
	protected void generatePojoClassFooter(TableDefinition table, JavaWriter out) {
		super.generatePojoClassFooter(table, out);
		JooqGeneratorTools.generateMerge(table, out);
	}

	@Override
	protected void generateDaos(SchemaDefinition schema) {
		m_currentSchema = schema;
		super.generateDaos(schema);
		m_currentSchema = null;
	}

	@Override
	protected boolean handleCustomTypeFromJson(TypedElementDefinition<?> column, String setter,
			String columnType, String javaMemberName, JavaWriter out) {
		// Support jOOQ 3.19 unsigned types that older jooq-vertx doesn't know about
		if (columnType.equals("org.jooq.types.UInteger")) {
			out.tab(2).println("setOrThrow(this::%s,name->UInteger.valueOf(json.getInteger(name)),\"%s\",\"%s\");", setter, javaMemberName, columnType);
			return true;
		}
		if (super.handleCustomTypeFromJson(column, setter, columnType, javaMemberName, out))
			return true;
			
		// upstream's custom type handler only handles types that can be mangled from JSON object or array
		// we can also handle strings, so try that
		var converter = findJsonConverter(column.getType());
		if (converter != null) {
			// in the future why might support more than just JSON strings, so there will need to be some kind of selector
			out.tab(2).println("setOrThrow(this::%s,name->new %s().jsonConverter().from(json.getString(name)),\"%s\",\"%s\");", setter, converter, javaMemberName, columnType);
			return true;
		}
		return false;
	}
	
	@Override
	protected boolean handleCustomTypeToJson(TypedElementDefinition<?> column, String getter, String columnType,
			String javaMemberName, JavaWriter out) {
		if (super.handleCustomTypeToJson(column, getter, columnType, javaMemberName, out))
			return true;

		// upstream's custom type handler only handles types that can be mangled to JSON object or array
		// we can also handle strings, so try that
		var converter = findJsonConverter(column.getType());
		if (converter != null) {
			// in the future why might support more than just JSON strings, so there will need to be some kind of selector
			out.tab(2).println("json.put(\"%s\",new %s().jsonConverter().to(%s()));", getJsonKeyName(column),converter,getter);
			return true;
		}
		return false;
	}
	
	private String findJsonConverter(DataTypeDefinition type) {
		var converterType = type.getConverter() != null ? type.getConverter() : type.getBinding();
		if (converterType == null)
			return null;
		try {
			var clz = Class.forName(converterType);
			if (JsonConverter.class.isAssignableFrom(clz))
				return converterType;
		} catch (ClassNotFoundException e) {
		}
		return null;
	}

}
