package io.cloudonix.lib.model.query;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.Record;
import org.jooq.Result;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import io.github.jklingsporn.vertx.jooq.shared.internal.QueryResult;
import jakarta.inject.Named;

public class ResultsCollector {

	private Result<Record> result;
	private List<QueryManager<?, ?>> list;

	public ResultsCollector(QueryResult result, List<QueryManager<?,?>> list) {
		this(result.<Result<Record>>unwrap(), list);
	}

	public ResultsCollector(Result<Record> result, List<QueryManager<?,?>> list) {
		this.result = result;
		this.list = list;
	}

	public ResultsCollector(Result<Record> result, QueryManager<?,?>...list) {
		this(result, Arrays.asList(list));
	}
	
	public ResultsCollector(QueryResult result, QueryManager<?,?>...list) {
		this(result.<Result<Record>>unwrap(), Arrays.asList(list));
	}
	
	public <T> List<T> toList(Class<? extends T> clazz) {
		return result.stream().map(r -> load(r,clazz)).collect(Collectors.toList());
	}

	public <T> T getOne(Class<? extends T> clazz) {
		return  result.stream().map(r -> load(r,clazz)).findFirst().orElse(null);
	}
	
	public <T1, U> Stream<U> map(Class<? extends T1> clz1, Function<T1,U> mapper) {
		return result.stream().map(r -> mapper.apply(load(r,clz1)));
	}
	
	public <T1, T2, U> Stream<U> map(Class<? extends T1> clz1, Class<?extends T2> clz2, BiFunction<T1,T2,U> mapper) {
		return result.stream().map(r -> mapper.apply(load(r,clz1), load(r,clz2)));
	}
	
	public <T1, T2, T3, U> Stream<U> map(Class<? extends T1> clz1, Class<? extends T2> clz2, Class<? extends T3> clz3,
			TriFunction<T1,T2,T3,U> mapper) {
		return result.stream().map(r -> mapper.apply(load(r,clz1), load(r,clz2), load(r,clz3)));
	}

	public <T1, T2, T3, T4, U> Stream<U> map(Class<? extends T1> clz1, Class<? extends T2> clz2, Class<? extends T3> clz3,
			Class<? extends T4> clz4, QuadFunction<T1,T2,T3,T4,U> mapper) {
		return result.stream().map(r -> mapper.apply(load(r,clz1), load(r,clz2), load(r,clz3), load(r,clz4)));
	}

	public <T1, T2, T3, T4, T5, U> Stream<U> map(Class<? extends T1> clz1, Class<? extends T2> clz2, Class<? extends T3> clz3,
			Class<? extends T4> clz4, Class<? extends T5> clz5, PentaFunction<T1,T2,T3,T4,T5,U> mapper) {
		return result.stream().map(r -> mapper.apply(load(r,clz1), load(r,clz2), load(r,clz3), load(r,clz4), 
				load(r,clz5)));
	}

	public <T1, T2, T3, T4, T5, T6, U> Stream<U> map(Class<? extends T1> clz1, Class<? extends T2> clz2, Class<? extends T3> clz3,
			Class<? extends T4> clz4, Class<? extends T5> clz5, Class<? extends T6> clz6, HexaFunction<T1,T2,T3,T4,T5,T6,U> mapper) {
		return result.stream().map(r -> mapper.apply(load(r,clz1), load(r,clz2), load(r,clz3), load(r,clz4), 
				load(r,clz5), load(r,clz6)));
	}

	public <T1, T2, T3, T4, T5, T6, T7, U> Stream<U> map(Class<? extends T1> clz1, Class<? extends T2> clz2, Class<? extends T3> clz3,
			Class<? extends T4> clz4, Class<? extends T5> clz5, Class<? extends T6> clz6, Class<? extends T7> clz7, 
			SeptaFunction<T1,T2,T3,T4,T5,T6,T7,U> mapper) {
		return result.stream().map(r -> mapper.apply(load(r,clz1), load(r,clz2), load(r,clz3), load(r,clz4), 
				load(r,clz5), load(r,clz6), load(r,clz7)));
	}

	@FunctionalInterface
	public static interface TriFunction<A,B,C,U> {
		U apply(A a, B b, C c);
	}

	@FunctionalInterface
	public static interface QuadFunction<A,B,C,D,U> {
		U apply(A a, B b, C c, D d);
	}

	@FunctionalInterface
	public static interface PentaFunction<A,B,C,D,E,U> {
		U apply(A a, B b, C c, D d, E e);
	}

	@FunctionalInterface
	public static interface HexaFunction<A,B,C,D,E,F,U> {
		U apply(A a, B b, C c, D d, E e, F f);
	}

	@FunctionalInterface
	public static interface SeptaFunction<A,B,C,D,E,F,G,U> {
		U apply(A a, B b, C c, D d, E e, F f, G g);
	}
	
	public <RECORD,U> Stream<U> recordMap(Class<RECORD> recordType, Function<RECORD,U> mapper) {
		return recordMap(() -> {
			try {
				return recordType.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				throw new RuntimeException(e);
			}
		}, mapper);
	}
	
	@SuppressWarnings("unchecked")
	public <RECORD,U> Stream<U> recordMap(Callable<RECORD> recordTypeCreator, Function<RECORD,U> mapper) {
		Class<RECORD> recordType;
		try {
			recordType = (Class<RECORD>) recordTypeCreator.call().getClass();
		} catch (Exception e) {
			throw new RuntimeException("Error creating custom record container!", e);
		}
		ArrayList<BiConsumer<Record,RECORD>> resolvers = new ArrayList<>();
		for (var f : recordType.getDeclaredFields()) {
			var clz = f.getType();
			if (isJooqPojo(clz)) {
				resolvers.add((r,o) -> mapHelperSafeFieldSet(f, o, load(r, clz)));
				continue;
			}
			
			// see if it is named
			var name = f.getAnnotation(Named.class);
			var field = DSL.field(name != null ? name.value() : f.getName());
			resolvers.add((r,o) -> {
				if (r.field(field) != null)
					mapHelperSafeFieldSet(f, o, r.get(field));
			});
		}
		return result.stream().map(r -> {
			try {
				var o = recordTypeCreator.call();
				resolvers.forEach(res -> res.accept(r,o));
				return mapper.apply(o);
			} catch (Exception e) {
				throw new RuntimeException("Error mapping using custom record container type",e);
			}
		});
	}
	
	private <P> P load(Record r, Class<P> type) {
		var rec = isolatedRecord(r, type);
		return rec != null ? rec.into(type) : null;
	}
	
	private Record isolatedRecord(Record r, Class<?> type) {
		var manager = findManager(type);
		if (Objects.isNull(manager))
			throw new DataAccessException("Failed to find query manager for " + type + " in join query");
		var table = manager.getTable();
		var hasIds = table.getPrimaryKey().getFields().stream().map(f -> r.get(f)).anyMatch(Objects::nonNull);
		return hasIds ? r.into(table) : null;
	}

	private QueryManager<?,?> findManager(Class<?> type) {
		for (var q : list) {
			if (q.getType().equals(type))
				return q;
		}
		return null;
	}
	
	private boolean isJooqPojo(Class<?> clz) {
		if (clz == null)
			return false; // Definitely not a jOOQ POJO
		var mngr = findManager(clz);
		if (mngr != null)
			return true;
		return isJooqPojo(clz.getSuperclass()); // super may be null
	}
	
	private static void mapHelperSafeFieldSet(Field f, Object inst, Object value) {
		try {
			f.setAccessible(true);
			f.set(inst, value);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException("Error mapping using custom record container type",e);
		}
	}

}
