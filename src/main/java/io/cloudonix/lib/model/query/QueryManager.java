package io.cloudonix.lib.model.query;

import java.util.*;

import org.jooq.*;
import org.jooq.impl.DSL;

import com.google.inject.Inject;

import io.github.jklingsporn.vertx.jooq.shared.internal.AbstractVertxDAO;
import io.vertx.core.Future;

public class QueryManager<R extends UpdatableRecord<R>, P> {

	AbstractVertxDAO<R, P, Integer, Future<List<P>>, Future<P>, Future<Integer>, Future<Integer>> dao;

	@Inject
	public QueryManager(
			AbstractVertxDAO<R, P, Integer, Future<List<P>>, Future<P>, Future<Integer>, Future<Integer>> dao) {
		this.dao = dao;
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> with(QueryManager<R2,P2> right) {
		return new JoinQuery<>(this, right, JoinType.JOIN, null);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> with(AbstractVertxDAO<R2,P2, Integer, Future<List<P2>>, Future<P2>, Future<Integer>, Future<Integer>> right) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.JOIN, null);
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> with(QueryManager<R2,P2> right, Condition on) {
		return new JoinQuery<>(this, right, JoinType.JOIN, on);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> with(AbstractVertxDAO<R2,P2, Integer, Future<List<P2>>, Future<P2>, Future<Integer>, Future<Integer>> right, Condition on) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.JOIN, on);
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> leftJoin(QueryManager<R2,P2> right) {
		return new JoinQuery<>(this, right, JoinType.LEFT_OUTER_JOIN, null);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> leftJoin(AbstractVertxDAO<R2,P2, Integer, Future<List<P2>>, Future<P2>, Future<Integer>, Future<Integer>> right) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.LEFT_OUTER_JOIN, null);
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> leftJoin(QueryManager<R2, P2> right, Condition on) {
		return new JoinQuery<>(this, right, JoinType.LEFT_OUTER_JOIN, on);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> leftJoin(AbstractVertxDAO<R2,P2, Integer, Future<List<P2>>, Future<P2>, Future<Integer>, Future<Integer>> right, Condition on) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.LEFT_OUTER_JOIN, on);
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> rightJoin(QueryManager<R2,P2> right) {
		return new JoinQuery<>(this, right, JoinType.RIGHT_OUTER_JOIN, null);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> rightJoin(AbstractVertxDAO<R2,P2, Integer, Future<List<P2>>, Future<P2>, Future<Integer>, Future<Integer>> right) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.RIGHT_OUTER_JOIN, null);
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> rightJoin(QueryManager<R2,P2> right, Condition on) {
		return new JoinQuery<>(this, right, JoinType.RIGHT_OUTER_JOIN, on);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> rightJoin(AbstractVertxDAO<R2,P2, Integer, Future<List<P2>>, Future<P2>, Future<Integer>, Future<Integer>> right, Condition on) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.RIGHT_OUTER_JOIN, on);
	}
	
	public static Condition reduceConditions(List<Condition> conditions) {
		if (conditions.isEmpty())
			return DSL.trueCondition();
		Condition first = conditions.get(0);
		return conditions.stream().skip(1).reduce(first, Condition::and);
	}

	public Table<R> getTable() {
		return dao.getTable();
	}

	public Class<P> getType() {
		return dao.getType();
	}
	
	public AbstractVertxDAO<R,P,Integer,Future<List<P>>,Future<P>,Future<Integer>, Future<Integer>> getDao() {
		return dao;
	}

}
