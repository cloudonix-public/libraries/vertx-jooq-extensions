# vertx-jooq-extensions

A set of extensions for the vertx-jooq project ( https://github.com/jklingsporn/vertx-jooq ) that we find useful and would like to share with the world (and our own projects).

# Usage:

## Generator Extensions

In your model project (where you generate the jOOQ model), add the following plugin repository:

```
<pluginRepositories>
  <pluginRepository>
    <id>cloudonix-dist</id>
    <url>https://cloudonix-dist.s3.amazonaws.com/maven2/releases</url>
  </pluginRepository>
</pluginRepositories>
```

Add the module as a dependency to the jOOQ generate plugin:

```
<plugin>
	<groupId>org.jooq</groupId>
	<artifactId>jooq-codegen-maven</artifactId>
	<version>3.11.11</version>
	<executions>
		<execution>
			<phase>compile</phase>
			<goals>
				<goal>generate</goal>
			</goals>
		</execution>
	</executions>

	<dependencies>
		<dependency>
			<groupId>io.cloudonix.lib</groupId>
			<artifactId>vertx-jooq-extensions</artifactId>
			<version>1.0.0</version>
		</dependency>
	</dependencies>

	<configuration>
	<!-- ... -->
	</configuration>
</plugin>
```

Then use one of the facilities provided here, as documented below:

### Extended Generator With Support for Concrete BI POJOs

The extended generator currently extends the vertx-jooq `CompletableFutureJDBCGuiceVertxGenerator`, as this
is what we use, but its only a few lines that call onto other classes to do "the dirty work", so we can easily add
support for other jOOQ custom generators if there is demand for that.

The extended generator allows the user to specify "concrete POJO BI classes" - i.e. classes that inherit from
a POJO class and extend its functionality with specific BI operations. This allows the application to more cleanly
perform BI logic by embedding it in the POJO class in an object-oriented way, instead of putting them in a utility
class and passing dumb POJOs back and forth. The generate will overwrite the DAO implementation to instantiate
concrete BI classes instead of dump POJOs, where appropriate.

To use this facility:

1. Configure the jOOQ codegen plugin to use the extended generator, instead of `io.github.jklingsporn.vertx.jooq.generate.completablefuture.CompletableFutureJDBCGuiceVertxGenerator`,
and also the extended strategy, instead of `VertxGeneratorStrategy`:

```
<generator>
	<name>io.cloudonix.lib.model.support.ExtendedCompletableFutureJDBCGuiceVertxGenerator</name>
	<strategy>
		<name>io.cloudonix.lib.model.support.ExtendedVertxGeneratorStrategy</name>
	</strategy>
...
```

2. Create a package for your concrete types. The default would be a sub package of what you configured for the
generated POJO types (i.e. what you configured under `generator` -> `target` -> `packageName`, plus
`tables.pojos`) named `concrete`. For example - if you configured the jOOQ generator with the package name
`com.example.model`, then the default concrete package name would be `com.example.model.tables.pojos.concrete`.
You can override this behavior by setting the system property `io.cloudonix.lib.model.concrete-package` to
whatever fully qualified package name makes for you. You may also configure an alternative source directory name for the concrete type, other than `src/main/java` if you so desire by setting the system property `io.cloudonix.lib.model.source-directory`.
3. Create "concrete" BI POJO classes under the concrete package, named exactly like the POJO classes they replace,
and extending the original POJO classes.

The generator will only change the DAO for POJO classes for which it finds a concrete BI type, to instantiate the
concrete type instead of the original POJO - so you don't need to create replacements for all of the POJOs generated,
only for those for which it makes sense.

#### Merge JSON Function

The custom generator also adds a new function to the POJO footer: `merge(JsonObject)`. This is a
utility for updating a POJO with the content of a compatible Json - which may not contain all the fields supported:
instead of setting missing fields to `null` as `fromJson(JsonObject)` would do, `merge(JsonObject)` first creates
a JSON that includes all the fields of the POJO overwritten by existing fields of the JSON object, then re-reads the
JSON using `fromJson(JsonObject)`.

#### Custom JSON Key Style

The extended strategy allows to customize the [JSON key name style](https://en.wikipedia.org/wiki/Letter_case#Special_case_styles),
when converting JSON to/from POJOs using the `toJson()`/`fromJson()` methods, instead of the Vert.x jOOQ
generator's default which is just the SQL column name.

To change the JSON key name style, set the system property `io.cloudonix.lib.model.json-key-style` to one of the
following values:

- `original` : do not change the key name style, use the column name as is (default).
- `camel` : use `camelCase` style.
- `camel-proper` : use `ProperCamelCase` style.
- `snake` : use `snake_case` style.
- `kebab` : use `kebab-case` style.

The type name is case insensitive and special character insensitive (i.e. "`camel-proper`", "`camel proper`" or "`CamelProper`" will all work).

## Joined Query Manager

The Cloudonix Query Manager allows the developer to easily create `JOIN` queries using vertx-jooq DAOs and extract multiple POJOs from the results.

To use the query manager, create query managers that wrap existing vertx-jooq daos (it works well with injection, or on the fly) and join them in a query using `with()` (inner join), `leftJoin()` or `rightJoin()`.
The query manager will automatically find the references between one table and another using the foreign key information in the jOOQ model. Note: the query manager currently only supports table whose primary key is a single integer field - this is a requirement of the simple relationship inferencer and may be removed in a future version - patch requests are welcome.

The query manager will return a `JoinQuery` type that offers methods to execute the query with a variety of search types, using:

- `findOneByCondition(Condition)`
- `findManyByCondition(Condition)`
- `findManyByCondition(Condition, OrderField...)`
- or even the flexible `query(select -> select.where(...))`

The resulting `CompletableFuture<ResultsCollector>` can be used to compose retrieve, list or map the result set to one or more POJOs.

### Examples:

#### Retrieving One POJO of a JOIN

This method is useful if you want to find a specific record by matching in a joined table:

```
new QueryManager<>(usersDao).with(aliasesDao).findOneByCondition(ALIASES.ALIAS.eq("nick"))
		.thenApply(res -> res.getOne(Users.class))
		.thenCompose(user -> ...);
```

The next completion stage receives a single user instance, or `null` if none where find, exactly like the regular
vertx-jooq `findOneByCondition()`.

#### Retrieving All POJOs of One Type From a JOIN

This method is useful to retrieve a list of POJOs according to a criterion on a related table, and is often
more efficient than using a query with a sub-select:

```java
new QueryManager<>(usersDao).with(departmentsDao).findManyByCondition(DEPARTMENTS.FLOOR.eq(3))
		.thenApply(res -> res.toList(Users.class))
		.thenCompose(users -> ...);
```

The next completion stage receives a `List<Users>`, as expected from the regular vertx-jooq
`findManyByCondition()`. Please note that the resulting list may be empty

#### Retrieve All POJOs From a JOIN

This method is useful to retrieve all corresponding POJOs in one go, by mapping the set of POJOs to another type, like a custom tupple (or the new JDK 14 `record` type):

```java
new QueryManager<>(usersDao).with(departmentsDao).findManyByCondition(DSL.noCondition())
		.thenApply(res -> res.map(Users.class, Departments.class, (u,d) -> {
			return new DepartmentUser(d,u);
		})
		.thenCompose(departmentUsers -> ...);
```

The next completion stage receives a `List<DepartmentUser>`, that will be useful to see which department
each user belongs to.

The mapper also automatically understand bean models - by providing the results collector with a type that has a public default constructor and bean setters for POJOs expected in the joined query, the query manager can create such objects and set them up with the appropriate fields. For example:

```java
new QueryManager<>(usersDao).with(departmentsDao).findManyByCondition(DSL.noCondition())
    .thenApply(res -> res.recordMap(DepartmentUser.class, u -> u))
    .thenCompose(departmentUsers -> ...);
```

The second parameter to `recordMap()` is a mapping function that receives the instance of the bean type and can normalize it - or just pass it along, as in the example.

### Custom `ON` Conditions

Sometimes the automatic reference discovery algorithm may fail to find the most appropriate reference between two tables, which is often the case when the tables have multiple cross-references and the correct reference to select is context-specific.

For example, if we have tables `A`, `B` and `C` such that `B` references to `A` but `C` references both `A` and `B`. When using `QueryManager` naively, one might write:

```java
new QueryManager<>(aDao).with(bDao).with(cDao)
```

which might produce a `JOIN` query like this: `... FROM a INNER JOIN b ON a.id = b.a_id INNER JOIN c ON b.id = c.b_id`, which may not be what we wanted - we might want
to find the `C` records that point to the corresponding `A` record.

To solve this problem, `QueryManager` allows to customize the `JOIN` condition by accepting a jOOQ `Condition` as an optional second argument. To solve the issue described
above, write:

```java
new QueryManager<>(aDao).with(bDao).with(cDao, C.A_ID.eq(A.ID))
```
